# cnf - a "command not found"-handler

cnf ist eine Kommandozeilenanwendung, die einem dabei hilft, Befehle "zu
finden". Dazu befragt es in erster Linie diverse Paketmanager (apt, dnf,
pacman, flatpak). Darüber hinaus unterstützt es die Ausführung von Befehlen in
toolbx (und bald auch distrobox) Containern und kann mit einfachen Skripten vom
Nutzer flexibel erweitert werden.

Hier befinden sich die "Folien" zu einem Vortrag, den ich am 22.06.2023 im
Rahmen der c14h beim NoName e.V. gehalten habe. Die Aufnahme zum Vortrag ist
hier verlinkt: https://www.noname-ev.de/chaotische_viertelstunde.html#c14h_582


## Ausführen

Die Präsentation wird in einem Container ausgeführt. Dazu benötigt man `podman`
oder `docker` auf dem Rechner. Der folgende Befehl lädt dann den Container
herunter oder baut ihn lokal und startet die Präsentation:

```
$ ./present.sh
```
