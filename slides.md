---
title: cnf - a "command not found"-handler
author: Andreas Hartmann (@hartan)
date: 2023.06.22
extensions: []
---

# Before we begin

Navigating this presentation:
- `l`/`j`/`SPACE`: Next slide
- `h`/`k`/`BACKSPACE`: Previous slide
- `q`: Quit presentation

To read a hyperlink (by default: underlined and colored blue), click it once to
reveal the URL. Click it again to hide the URL.


---
# Agenda for today

- Chaos!
<!-- stop -->
- And a live demo


---
# About the project

- **Purpose**: Replace pesky "command not found" messages
<!-- stop -->


- Started out in January 2022
- [Developed on Gitlab](https://gitlab.com/hartan/cnf) (by me)
- Been through 4 or 5 full rewrites so far...
    - Initially a *bash*-script
    - Then a more refined *bash*-script
    - Ported to *rust*
<!-- stop -->


- Initially focused on [Sliverblue](https://fedoraproject.org/silverblue/) users
    - Because of the [toolbx](https://containertoolbx.org/) utility
    - First few version were mere "translation"-layers
    - Today: much broader scope ("be useful for any linux distro")


---
# Motivation (part 1)

- You're working on your Silverblue/Kinoite/... host
<!-- stop -->
- You want to monitor your CPU/memory usage
<!-- stop -->
- You open a tool such as `htop`:

```bash
$ htop
bash: htop: command not found
```
<!-- stop -->

- You remember that you just installed `htop` inside a toolbx container:

```bash
$ toolbox run -c my-awesome-toolbx htop
```


---
# Motivation (part 2)

- Imagine you're in a shell on a different PC...
<!-- stop -->
- You want to monitor your CPU/memory usage
<!-- stop -->
- You open a tool such as `htop`:

```bash
$ htop
bash: htop: command not found
```
<!-- stop -->

- Humm... You want to install `htop`:

```bash
$ sudo dnf -y install htop
sudo: dnf: command not found
```
<!-- stop -->

- Right, this is an Ubuntu machine...

```bash
$ sudo apt search htop
# 500 results and a lot of scrolling later...
$ sudo apt install htop
```


---
# Motivation (part 3)

**There are recurring patterns**:

## For Silverblue users
    1. Enter a `command`
    2. Shell reports "command not found"
    3. Remember if/where you installed `command`
    4. Perform the translation (`toolbox run ...`)
<!-- stop -->


## For users of most Linux distributions
    1. Enter a `command`
    2. Shell reports "command not found"
    3. Remember how to operate your package manager correctly
    4. Find package that provides `command`
    5. Install `command`
    6. Rerun `command`
<!-- stop -->


This gets worse for Silverblue users:
- You can run all these steps on your host
<!-- stop -->
- And in any of the `toolbx` containers you have as well...


---
# `cnf` as a solution to these problems/patterns
<!-- stop -->

## Perform command lookup in various providers

- Query known package managers
    - Preferrably using cached results if possible...
    - Directly ask "which package provides `command`?"
<!-- stop -->
- Allow users to bring their own
    - Either while they wait for inclusion in the main project
    - Or to perform arbitrary lookups (e.g. `find ...`)
<!-- stop -->

## Perform command translation between execution environments

- When on host, prepend the equivalent of `toolbox run -c`
- When in a toolbx container, prepend the equivalent of `flatpak-spawn --host`
<!-- stop -->

> And combine both: Query all known providers on the host and in a toolbx container...

---
# More features of `cnf`

## Remember command translations (`alias`)

- Some commands aren't in `$PATH` (e.g. flatpak apps)
- Some commands should always be run in a different execution environment
- cnf `alias`es provide a good integration of both into your terminal
<!-- stop -->

## Shell integration

- Run `cnf` automatically whenever a command wasn't found
- Works by hooking into your shell


---
# Future Plans

- Add more providers (maybe `nix` and others)
- Add more environments (POC for
  [distrobox](https://github.com/89luca89/distrobox) exists already)
- Support other Platforms than Linux (Mac, Windows, ...?)
- Allow "integration" into regular shell scripts
- Tighter container integration (maybe add a `cnfd`?)

---
# The End

Thanks for your attention. :)

[silverblue]: https://fedoraproject.org/silverblue/
[cnf-gitlab]: https://gitlab.com/hartan/cnf
[toolbx]: https://containertoolbx.org/
